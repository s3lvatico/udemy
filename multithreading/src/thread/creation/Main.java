package thread.creation;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        Thread t = new Thread(
                () -> {
                    Thread current = Thread.currentThread();
                    System.out.printf("[%15d | %s] Ciao io sono il thread <%s>, id %d%n", System.currentTimeMillis(), current.getName(), current.getName(), current.getId());
                    System.out.printf("[%15d | %s] priorità del thread: %d%n", System.currentTimeMillis(), current.getName(), current.getPriority());
                    try {
                        TimeUnit.SECONDS.sleep(1);
                        System.out.printf("[%15d | %s] fine del thread%n", System.currentTimeMillis(), current.getName());
                    } catch (InterruptedException ie) {
                        ie.printStackTrace();
                    }
                }
        );
        t.setName("worker thread");
        t.setPriority(Thread.MAX_PRIORITY);
        System.out.printf("[%15d] Siamo nel thread <%s> prima di avviare un nuovo thread%n", System.currentTimeMillis(), Thread.currentThread().getName());
        t.start();
        System.out.printf("[%15d] Siamo nel thread <%s> dopo l'avvio di un nuovo thread%n", System.currentTimeMillis(), Thread.currentThread().getName());
    }
}
