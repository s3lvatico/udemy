package thread.creation.example;

import java.util.concurrent.TimeUnit;

public class HandlingThreadExceptionsMain {

    public static void main(String[] args) {
        Thread thread = new Thread(
                () -> {
                    System.out.printf("[%s] thread is active%n", Thread.currentThread().getName());
                    throw new RuntimeException("Intentional exception");
                }
        );

        thread.setName("pathologic thread");

        thread.setUncaughtExceptionHandler(
                (t, e) -> {
                    System.err.printf("-!- A critical error was signaled by the thread [%s]%n", t.getName());
                    System.err.printf("-!- Error message is <%s>%n", e.getMessage());
                }
        );

        System.out.printf("[%s] before starting new thread%n", Thread.currentThread().getName());
        thread.start();
        System.out.printf("[%s] after starting new thread%n", Thread.currentThread().getName());

//        try {
//            TimeUnit.MILLISECONDS.sleep(1000);
//        }
//        catch (InterruptedException e) {
//            System.err.println("-!- sleep interrupted");
//        }
    }
}
