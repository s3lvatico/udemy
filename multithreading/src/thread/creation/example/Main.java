package thread.creation.example;

import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        Thread thread = new Thread(
                () -> {
                    System.out.printf("Messaggio dal thread <%s>%n", Thread.currentThread().getName());
                    System.out.printf("[%s] priorità: %s%n", Thread.currentThread().getName(), Thread.currentThread().getPriority());
                }
        );
        thread.setName("My worker thread");
        thread.setPriority(Thread.MAX_PRIORITY);
        System.out.printf("Siamo nel thread <%s> prima di avviare un nuovo thread%n", Thread.currentThread().getName());
        thread.start();
        System.out.printf("Siamo nel thread <%s> dopo l'avvio di un nuovo thread%n", Thread.currentThread().getName());
        try {
            TimeUnit.SECONDS.sleep(1);
        } catch (InterruptedException e) {}
    }
}
