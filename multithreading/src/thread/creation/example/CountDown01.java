package thread.creation.example;

import java.util.concurrent.TimeUnit;

public class CountDown01 {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
           int count = 10;
           while (count >= 0) {
               System.out.printf("[%s]<%d> ", Thread.currentThread().getName(), count--);
               try {
                   TimeUnit.MILLISECONDS.sleep(500l);
               }
               catch (InterruptedException e) {}
           }
            System.out.println("Bum!");
        });

        System.out.printf("[%s] prima dell'avvio del nuovo thread%n", Thread.currentThread().getName());
        thread.start();
        System.out.printf("[%s] dopo l'avvio del nuovo thread%n", Thread.currentThread().getName());

    }
}
