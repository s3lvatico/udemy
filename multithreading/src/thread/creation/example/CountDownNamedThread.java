package thread.creation.example;

import java.util.concurrent.TimeUnit;

public class CountDownNamedThread {

    public static void main(String[] args) {
        Thread thread = new Thread(() -> {
            Thread currentThread = Thread.currentThread();
            System.out.printf("[%s] this thread has priority <%s>%n", currentThread.getName(), currentThread.getPriority());
            int count = 10;
            while (count >= 0) {
                System.out.printf("[%s]<%d> ", Thread.currentThread().getName(), count--);
                try {
                    TimeUnit.MILLISECONDS.sleep(500l);
                } catch (InterruptedException e) {
                }
            }
            System.out.println("\nBum!");
        });
        thread.setName("countDownThread");
        thread.setPriority(Thread.MAX_PRIORITY);
        System.out.printf("[%s] prima dell'avvio del nuovo thread%n", Thread.currentThread().getName());
        thread.start();
        System.out.printf("[%s] dopo l'avvio del nuovo thread%n", Thread.currentThread().getName());

    }
}
