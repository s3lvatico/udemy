package thread.creation.example2;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Main {

    static final int MAX_PASSWORD = 9999;

    /**
     * crea 2 thread di hacker e un thread per la polizia, li avvia tutti insieme e vediamo che accade.
     * Naturalmente i due thread hacker condividono la stessa {@link Vault}
     *
     * @param args
     */
    public static void main(String[] args) {
        Random rnd = new Random();
        Vault vault = new Vault(rnd.nextInt(MAX_PASSWORD));
        ExecutorService executor = Executors.newCachedThreadPool();
        executor.execute(new AscendingHackerThread(vault));
        executor.execute(new DescendingHackerThread(vault));
        executor.execute(new PoliceThread());
        executor.shutdown();

    }

    /**
     * una cassaforte con combinazione.
     * <p>vedi {@link #isCorrectPassword(int)} per provare una combinazione e sapere se è giusta</p>
     */
    private static class Vault {

        private int password;

        /**
         * crea una nuova cassaforte con la password specificata
         *
         * @param password
         */
        Vault(int password) {
            this.password = password;
        }

        /**
         * controlla se la password specificata apre o no la cassaforte
         *
         * @param guess
         * @return
         */
        boolean isCorrectPassword(int guess) {
            try {
                TimeUnit.MILLISECONDS.sleep(5);
            } catch (InterruptedException e) {
            }
            return guess == password;
        }
    }

    /**
     * classe base degli hacker; riceve una {@link Vault} e prova in qualche modo ad aprirla
     */
    static abstract class HackerThread extends Thread {
        protected Vault vault;

        HackerThread(Vault vault) {
            this.vault = vault;
            setName(getClass().getSimpleName());
            setPriority(Thread.MAX_PRIORITY);
        }

        protected void tryOpenTheVault(int guess) {
            if (vault.isCorrectPassword(guess)) {
                System.out.println(getName() + " opened the vault: " + guess);
                System.exit(0);
            }
        }

    }

    /**
     * un {@link HackerThread} che prova ad aprire la {@link Vault} con password che via via
     * diventano più grandi, a partire da zero
     */
    static class AscendingHackerThread extends HackerThread {

        AscendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int guess = 0; guess < MAX_PASSWORD; guess++) {
                tryOpenTheVault(guess);
            }
        }
    }

    /**
     * un {@link HackerThread} che prova ad aprire la {@link Vault} con password che partono
     * da un valore massimo e decrescono via via.
     */
    static class DescendingHackerThread extends HackerThread {

        DescendingHackerThread(Vault vault) {
            super(vault);
        }

        @Override
        public void run() {
            for (int guess = MAX_PASSWORD; guess >= 0; guess--) {
                tryOpenTheVault(guess);
            }
        }
    }

    /**
     * Il thread dei poliziotti. Dà tempo 10 secondi ai vari hacker per aprire la {@link Vault},
     * dopodiché li sgama tutti (chiudendo l'applicazione ex abrupto).
     */
    static class PoliceThread extends Thread {

        public void run() {
            for (int i = 10; i > 0; i--) {
                System.out.println(i);
                try {
                    TimeUnit.MILLISECONDS.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
            System.out.println("Game over for hackers!");
            System.exit(0);
        }
    }

}
