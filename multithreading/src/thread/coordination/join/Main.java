package thread.coordination.join;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class Main {

    public static void main(String[] args) {
        List<Long> inputNumbers = Arrays.asList(0L, 3435L, 35435L, 2324L, 4656L, 23L, 2435L, 5566L, 1000000L);

        List<FactorialThread> threads = new ArrayList<>();

        inputNumbers.stream()
                .map(l -> {
                    FactorialThread t = new FactorialThread(l);
                    // impostare il thread come daemon assicura che il thread principale non si
                    // imballi in attesa che i thread a completamento più lungo terminino
                    //t.setDaemon(true);
                    return t;
                })
                .forEach(threads::add);

        threads.forEach(t -> t.start());

        threads.forEach(t -> {
            try {
                t.join(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            if (t.isFinished()) {
                System.out.printf("factorial of %d is %10.10s...%n", t.getInputNumber(), t.getResult());
            } else {
                System.out.printf("calculation for %d is still in progress...", t.getInputNumber());
            }
        });

    }

}
