package thread.coordination.join;

import java.math.BigInteger;

class FactorialThread extends Thread {

    private long inputNumber;
    private BigInteger result = BigInteger.ZERO;
    private boolean finished = false;

    FactorialThread(long inputNumber) {
        this.inputNumber = inputNumber;
    }

    public BigInteger factorial(long n) {
        BigInteger tempResult = BigInteger.ONE;
        for (long i = n; i > 0; i--) {
            tempResult = tempResult.multiply(new BigInteger(Long.toString(i)));
        }
        return tempResult;
    }

    @Override
    public void run() {
        result = factorial(inputNumber);
        finished = true;
    }

    public boolean isFinished() {
        return finished;
    }

    public BigInteger getResult() {
        return result;
    }

    public long getInputNumber() {
        return inputNumber;
    }
}
