package thread.interrupt;

import java.util.concurrent.TimeUnit;

public class InterruptibleTaskMain {

    public static void main(String[] args) {
        Thread thread = new Thread(new BlockingTask());
        thread.setName("blockable");
        
        thread.start();

        try {
			TimeUnit.SECONDS.sleep(3L);
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}

        thread.interrupt();

    }

    static class BlockingTask implements Runnable {

        @Override
        public void run() {
            try {
                TimeUnit.SECONDS.sleep(60);
            } catch (InterruptedException e) {
                System.err.println("Thread interrupted");
            }
        }
    }
}
