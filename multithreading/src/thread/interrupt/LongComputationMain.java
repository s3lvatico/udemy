package thread.interrupt;

import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

public class LongComputationMain {

    public static void main(String[] args) {
        Thread t = new Thread(new LongComputationTask(new BigInteger("2345"), new BigInteger(("123455234"))));

        // t.setDaemon(true); // in questo caso il thread non sarà più bloccante rispetto all'uscita del thread principale

        t.start();

        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
        }

        t.interrupt();
    }

    static class LongComputationTask implements Runnable {

        private BigInteger base;
        private BigInteger pow;

        public LongComputationTask(BigInteger base, BigInteger pow) {
            this.base = base;
            this.pow = pow;
        }

        private BigInteger pow(BigInteger base, BigInteger pow) {
            BigInteger result = BigInteger.ONE;

            for (BigInteger i = BigInteger.ZERO; i.compareTo(pow) != 0; i = i.add(BigInteger.ONE)) {
                result = result.multiply(base);

                if (Thread.currentThread().isInterrupted()) {
                    System.err.println(Thread.currentThread().getName() + " : Interrupt signal received. Exiting.");
                    return BigInteger.ZERO;
                }
            }
            return result;
        }

        @Override
        public void run() {
            System.out.printf("%s^%s = %s%n", base, pow, pow(base, pow));
        }


    }
}
